
import './App.css';
import Header from './components/Header';
import Hero from './components/Hero';
import Products from './components/Products';
import Footer from './components/Footer';
import Category from './components/Category';
import Ad from './components/Ad';
import { black, white } from "./colors";
import { ThemeProvider, createTheme } from "@mui/material/styles";


const theme = createTheme({
  palette: {
    primary: {
      main: black[900],
    },
    secondary: {
      main: white[900],
    }
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          margin: 0,
          fontFamily: 'Montserrat',
          WebkitFontSmoothing: 'antialiased',
          MozOsxFontSmoothing: 'grayscale',
          borderRadius: '50px',
          fontSize: '1.2rem',
        },
      },
    },
    MuiInputBase: {
      styleOverrides: {
        root: {
          margin: 0,
          fontFamily: 'Montserrat',
          WebkitFontSmoothing: 'antialiased',
          MozOsxFontSmoothing: 'grayscale',
          fontSize: '1rem',
        },
      },
    },
  },
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <div className="testbuit">
        <div className='cart-container'>
          <Header/>
        </div>
        <div className='hero-container'>
          <Hero/>
        </div>
        <div className='product-container'>
          <Products/>
        </div>  
        <div className='category-container'>
          <Category/>
        </div>
        <div className='ad-container'>
          <Ad/>
        </div>
        <div className='footer-container'>
          <Footer/>
        </div>
      </div>
    </ThemeProvider>
  );
}

export default App;
