import React from "react";
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';
import ad from '../assets/images/ad.png';
import "../Stylesheets/Ad.css"

function Ad(){
    return(
        <div className="container-ad">
            <Typography className="ad-title" variant="h1" sx={{fontSize: { xs: '50px', sm: '100px' } }}>
                50% DE DESCUENTO
            </Typography>
            <Typography className="sub-title" variant="h2" sx={{fontSize: { xs: '20px', sm: '30px' } }}>
                En productos Apple seleccionados
            </Typography>
            <Stack 
                className="buttons"
                alignItems="center"
                justifyContent={{ xs: 'center', sm: 'center' }}
                direction={{ xs: 'column', sm: 'row' }} 
                spacing={{ xs: 2 }}
            >
                <img 
                    className="image-ad-size"
                    src={ad}
                    alt="ad"
                />
                <Button 
                    color="primary" 
                    variant="contained" 
                    sx={{
                        marginRight: { sm: '10px' },
                        borderRadius: { xs: '10px' }  
                    }}
                >
                    Ver más
                </Button>
            </Stack> 
        </div>     
    )
}

export default Ad;