import React from "react";
import cart from '../assets/icons/cart.png'

function Header() {
    return (
        
        <img className="cart"
            src={cart}
            alt="Cart"
        />
        
    )
}

export default Header;
