import React from "react";
import logo from '../assets/images/TEST.png'
import downloadApp from '../assets/icons/image 8.png'
import WhatsAppIcon from '@mui/icons-material/WhatsApp';
import '../Stylesheets/Footer.css'

function Footer() {
    return (
        <footer>
            <div className="footer-links">
                <img className="logo-footer"
                    src={logo}
                    alt="Logo"
                />
                <h4>Descarga el app ya!</h4>
                <img className="downloadApp"
                    src={downloadApp}
                    alt="Logo"
                />
                <div className="footer-global">
                    <div className="footer-general">
                        <h3 className="footer-category">Comprar</h3>
                        <p className="category-option">Registrarse</p>
                        <p className="category-option">Iniciar sesión</p>
                        <p className="category-option">Categorías</p>
                        <p className="category-option">Marcas</p>
                    </div>
                    <div className="footer-general">
                        <h3 className="footer-category">Acerca</h3>
                        <p className="category-option">¿Quienes somos?</p>
                        <p className="category-option">Vender con Buit</p>
                        <p className="category-option">Programas de afiliados</p>
                        <p className="category-option">Guía para nuevos usuarios</p>
                    </div>
                    <div className="footer-general">
                        <h3 className="footer-category">Atención al cliente</h3>
                        <p className="category-option">Preguntas frecuentes</p>
                        <p className="category-option">Términos y condiciones</p>
                        <div className="contact">  
                            <WhatsAppIcon className="whatsapp" fontSize="small"/><p className="category-option">Contáctenos</p>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
            <div>
              <img className="logo-footer-test"
                src={logo}
                alt="Logo"
            />
            </div>
        </footer>   
    )
}

export default Footer;