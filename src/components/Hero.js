import React from "react";
import logo from '../assets/images/TEST.png'
import { styled, alpha } from '@mui/material/styles';
import InputBase from '@mui/material/InputBase';
import SearchIcon from '@mui/icons-material/Search';
import downloadApp from '../assets/icons/image 8.png'
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import MenuIcon from '@mui/icons-material/Menu';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import '../Stylesheets/Hero.css'

const Search = styled('div')(({ theme }) => ({
    position: 'relative',
    borderRadius: '50px',
    backgroundColor: theme.palette.common.white,
    '&:hover': {
        backgroundColor: alpha(theme.palette.common.white, 0.9),
    },
    marginLeft: 0,
    height: '50%',
    width: '100%',
    [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(1),
        width: 'auto',
    },
}));
  
const SearchIconWrapper = styled('div')(({ theme }) => ({
    padding: theme.spacing(0, 2),
    marginTop: '10px',
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'top',
    justifyContent: 'end',
}));
  
const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)})`,
        transition: theme.transitions.create('width'),
        width: '100%',
    },
}));

const drawerWidth = 240;
const navItems = ['Categorías', 'Quienes Somos', 'Contacto'];

function Hero(props) {
    const { window } = props;
    const [mobileOpen, setMobileOpen] = React.useState(false);
  
    const handleDrawerToggle = () => {
      setMobileOpen((prevState) => !prevState);
    };
  
    const drawer = (
      <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
        <List>
          {navItems.map((item) => (
            <ListItem key={item} disablePadding>
              <ListItemButton sx={{ textAlign: 'center' }}>
                <ListItemText primary={item} />
              </ListItemButton>
            </ListItem>
          ))}
        </List>
      </Box>
    );
  
    const container = window !== undefined ? () => window().document.body : undefined;

    return (
        <div className="hero">
            <Box sx={{ display: 'flex' }}>
                <AppBar color="transparent" position="relative" component="nav" sx={{ boxShadow: "none" }}>
                    <Toolbar>
                        <IconButton
                            color="secondary"
                            aria-label="open drawer"
                            edge="start"
                            onClick={handleDrawerToggle}
                            sx={{ mr: 2, display: { sm: 'none' } }}
                        >
                            <MenuIcon />
                        </IconButton>

                        <Typography
                            variant="h6"
                            component="div"
                            sx={{ display: { xs: 'none', sm: 'block' } }}
                        >
                            <img className="logo"
                                src={logo}
                                alt="Logo"
                            />
                        </Typography>

                        <Box sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}>
                            {navItems.map((item) => (
                            <Button key={item} sx={{ color: '#fff' }}>
                                {item}
                            </Button>
                            ))}
                        </Box>

                        <Search sx={{ height: { xs: '70%', sm: '50%'}, width: { xs: '70%', sm: '30ch' } }}>
                            <SearchIconWrapper>
                                <SearchIcon />
                            </SearchIconWrapper>
                            <StyledInputBase
                                placeholder="Qué estas buscando?"
                                inputProps={{ 'aria-label': 'search' }}
                            />
                        </Search>
                    </Toolbar>
                </AppBar>

                <Box component="nav">
                    <Drawer
                        container={container}
                        variant="temporary"
                        open={mobileOpen}
                        onClose={handleDrawerToggle}
                        ModalProps={{
                            keepMounted: true, // Better open performance on mobile.
                        }}
                        sx={{
                            display: { xs: 'block', sm: 'none' },
                            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
                        }}
                        >
                        {drawer}
                    </Drawer>
                </Box>
            </Box>

            <Stack  
                className="hero-text" 
                justifyContent="left" 
                direction="column" 
                spacing={0} 
                sx={{
                    fontSize: { xs: '85%', sm: '150%' },
                    marginTop: { xs: '10px', sm: '100px' },
                    marginLeft: { xs: '10px', sm: '30px' },
                    marginRight: { xs: '10px', sm: '30px' },
                    inlineSize: { xs: '100%', sm: '50%' }
                }}
            >
                <h1>Test App agrupa todas tus necesidades creativas en un mismo lugar</h1>
                <div className="subtitle-container">
                    <p>Test es una aplicación creada con el fin de hacerle una prueba a nuestros futuros programadores</p>
                </div>
            </Stack>
            
            <Stack 
                className="buttons"
                alignItems="center"
                justifyContent={{ xs: 'center', sm: 'space-between' }}
                direction={{ xs: 'column', sm: 'row' }} 
                spacing={{ xs: 1, sm: 2, md: 4 }}
            >
                <Stack 
                    alignItems="center"
                    justifyContent={{ xs: 'center', sm: 'left' }}
                    direction={{ xs: 'column', sm: 'row' }} 
                    spacing={{ xs: 1, sm: 2 }}
                >
                    <Button color="primary" variant="contained" sx={{marginRight: { sm: '10px' } }}>Únete</Button>
                    <Button color="secondary" variant="contained">Iniciar sesión</Button>
                </Stack>
                <img className="download-buttons"
                    src={downloadApp}
                    alt="Download buttons"
                />
            </Stack>
        </div>
    );
}

export default Hero;