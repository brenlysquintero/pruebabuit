import React from "react";
import iphone from '../assets/images/iphone.png';
import table from '../assets/images/wacom.png';
import pc from '../assets/images/mac.png';
import'../Stylesheets/Products.css'
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
    paritialVisibilityGutter: 60
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
    paritialVisibilityGutter: 50
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    paritialVisibilityGutter: 30
  }
};

function Products({ deviceType }) {
 return (
        <div>
            <h1 className="title">Productos destacados</h1>
            <div className="carousel">
                <Carousel
                    ssr
                    partialVisible
                    deviceType={deviceType}
                    itemClass="image-item"
                    responsive={responsive}
                    infinite={true}
                >
                    <div className="global-item-carousel">
                        <div className="back"><img
                            alt="img1"
                            draggable={false}
                            className="size-image"
                            src={iphone}
                        /></div>
                        <p className="paragraph">Iphone</p>
                    </div>
                    <div className="global-item-carousel">
                        <div className="back">
                        <img
                            alt="img1"
                            draggable={false}
                            className="size-image"
                            src={table}
                        /></div>
                        <p className="paragraph">Wacom</p>
                    </div>
                    <div className="global-item-carousel">                    
                        <div className="back">
                        <img
                            alt="img1"
                            draggable={false}
                            className="size-image"
                            src={pc}
                        />
                        </div>
                        <p className="paragraph">Macbook</p>
                     </div>
                </Carousel>
            </div>
            <button type="button" className="btn">
                Compra
            </button>
        </div>
    
        )
}

export default Products;