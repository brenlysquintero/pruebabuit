import React from "react";
import pc from '../assets/icons/pc.png';
import tlf from '../assets/icons/tlf.png';
import tablet from '../assets/icons/table.png';
import watch from '../assets/icons/watch.png';
import spray from '../assets/icons/spray.png';
import'../Stylesheets/Products.css'
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import "../Stylesheets/Category.css"

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 4,
    partialVisibilityGutter: 60
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
    partialVisibilityGutter: 50
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    partialVisibilityGutter: 30
  }
};

function Category({ deviceType }) {
 return (
        <div>
            <h1 className="title2">Categorías</h1>
            <div className="">
                <Carousel
                    ssr
                    partialVisible
                    deviceType={deviceType}
                    itemClass="image-item2"
                    responsive={responsive}
                    infinite={true}
                >
                    <div className="carousel-item">
                        <div className="back2">
                            <img
                                alt="img1"
                                draggable={false}
                                className="size-image2"
                                src={pc}
                            />
                        </div>
                        <p className="paragraph2">Computadoras</p>
                    </div>
                    <div className="carousel-item">
                        <div className="back2">
                            <img
                                alt="img1"
                                draggable={false}
                                className="size-image2"
                                src={tlf}
                            />
                        </div>
                        <p className="paragraph2">Teléfonos</p>
                    </div>
                    <div className="carousel-item">                    
                        <div className="back2">
                            <img
                                alt="img1"
                                draggable={false}
                                className="size-image2"
                                src={watch}
                            />
                        </div>
                        <p className="paragraph2">Accesorios</p>
                     </div>
                     <div className="carousel-item">                    
                        <div className="back2">
                            <img
                                alt="img1"
                                draggable={false}
                                className="size-image2"
                                src={tablet}
                            />
                        </div>
                        <p className="paragraph2">Tablets</p>
                     </div>
                    <div className="carousel-item">                    
                        <div className="back2">
                            <img
                                alt="img1"
                                draggable={false}
                                className="size-image2"
                                src={spray}
                            />
                        </div>
                        <p className="paragraph2">Pintura</p>
                     </div>
                </Carousel>
            </div>
        </div>
    )
}

export default Category;