export const black = {
    900: "#202020"
};
  
export const white = {
    50: "#FFFFFF",
    500: "#FBFBFB",
    900: "#F1F1F1"
};